import org.junit.Assert;
import org.junit.Test;


public class TaboadaTest {

	@Test
	public void deveRetornarTaboadaDois() {
		Taboada taboada = new Taboada();
		String mock = "2, 4, 6, 8, 10, 12, 14, 16, 18, 20";
		
		String result = taboada.retornaTaboada(2);
		
		Assert.assertEquals(mock, result);
	}
}
