import org.junit.Assert;
import org.junit.Test;


public class TrocaValoresTest {

	@Test
	public void deveRetornarValoresTrocadosConcatenados() {
		TrocaValores trocaValores = new TrocaValores();
		
		String result = trocaValores.trocaConcatenaNome("Mestre do Universo", "Jonatas");
		
		Assert.assertEquals("Jonatas Mestre do Universo", result);
		
	}
	
}
