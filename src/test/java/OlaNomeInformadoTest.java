import org.junit.Test;

import junit.framework.Assert;

public class OlaNomeInformadoTest {

	@Test
	public void deveRetornarOlaSeguidoNomeTest() {
		
		OlaNomeInformado instance = new OlaNomeInformado();
		
		String result = instance.criarSaudacao("Antonio");
		
		Assert.assertEquals("Ol� Antonio", result);
	}
	
}
