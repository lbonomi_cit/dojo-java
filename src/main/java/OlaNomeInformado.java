import java.util.Scanner;

public class OlaNomeInformado {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite seu nome:");
		String nome = s.nextLine();
		
		System.out.println(criarSaudacao(nome));
	}
	
	public static String criarSaudacao(String string) {
		return "Ol� " + string;
	}
}
