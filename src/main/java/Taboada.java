import java.util.Scanner;

public class Taboada {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite seu numero:");
		int numero = s.nextInt();
		
		System.out.println(retornaTaboada(numero));

	}

	public static String retornaTaboada(int numero) {
		String result = "";

		for (int i = 1; i <= 10; i++) {
			result = result + (numero * i);
			if (i < 10) {
				result = result + ", ";
			}
		}

		return result;
	}

}
