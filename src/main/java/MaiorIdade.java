
public class MaiorIdade {

	public static void main(String[] args) {

		Pessoa[] pessoas = new Pessoa[10];
		int m = 0, f = 0;
		
		pessoas[0] = new Pessoa();
		
		pessoas[0].setGenero("f");
		pessoas[0].setIdade(20);
		pessoas[0].setNome("Joana");
		
		pessoas[1] = new Pessoa();
		
		pessoas[1].setGenero("m");
		pessoas[1].setIdade(4);
		pessoas[1].setNome("Luan");
		
		pessoas[2] = new Pessoa();
		
		pessoas[2].setGenero("f");
		pessoas[2].setIdade(30);
		pessoas[2].setNome("Mari");
		
		pessoas[3] = new Pessoa();
		
		pessoas[3].setGenero("f");
		pessoas[3].setIdade(34);
		pessoas[3].setNome("Dani");
		
		pessoas[4] = new Pessoa();
		
		pessoas[4].setGenero("m");
		pessoas[4].setIdade(39);
		pessoas[4].setNome("Joao");
		
		pessoas[5] = new Pessoa();
		
		pessoas[5].setGenero("f");
		pessoas[5].setIdade(29);
		pessoas[5].setNome("Carol");
		
		pessoas[6] = new Pessoa();
		
		pessoas[6].setGenero("m");
		pessoas[6].setIdade(20);
		pessoas[6].setNome("Falipe");
		
		pessoas[7] = new Pessoa();
		
		pessoas[7].setGenero("m");
		pessoas[7].setIdade(22);
		pessoas[7].setNome("Pedro");
		
		pessoas[8] = new Pessoa();
		
		pessoas[8].setGenero("f");
		pessoas[8].setIdade(27);
		pessoas[8].setNome("Paloma");
		
		pessoas[9] = new Pessoa();
		
		pessoas[9].setGenero("m");
		pessoas[9].setIdade(3);
		pessoas[9].setNome("Manuel");
		
		for (int i = 0; i < pessoas.length; i++) {
			if(pessoas[i].getIdade() >= 18) {
				if(pessoas[i].getGenero().equals("m")) {
					m++;
				} else {
					f++;
				}
			}
		}
		
		System.out.println("Quantidade de homens: " + m);
		System.out.println("Quantidade de mulheres: " + f);
		
	}
}
