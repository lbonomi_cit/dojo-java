import java.util.Scanner;

public class TrocaValores {

	public static void main(String[] args) {
		
		String nome1 = "mestre do universo";
		String nome2 = "";
		
		System.out.println("Por favor digite o nome:");
		Scanner s = new Scanner(System.in);
		
		nome2 = s.nextLine();
		
		String result = trocaConcatenaNome(nome1, nome2);
		
		System.out.println(result);
		
	}
	
	public static String trocaConcatenaNome(String nome1, String nome2) {
		String aux = nome1;
		
		System.out.println("nome1 = " + nome1);
		System.out.println("nome2 = " + nome2);
		
		nome1 = nome2;
		nome2 = aux;
		
		System.out.println("nome1 = " + nome1);
		System.out.println("nome2 = " + nome2);
		
		return nome1 + " " + nome2;
	}
}
